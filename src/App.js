import React from 'react';
import List from './containers/List/list';

class App extends React.Component {
  state = {
    list: [],
    loading: false
  };

  componentDidMount() {
    this.setState({
      loading: true
    });
    fetch('http://localhost:8000/list')
      .then(r => r.json())
      .then(data => this.setState({ list: data, loading: false }));
  }

  render() {
    const { list, loading } = this.state;
    return <List list={list} loading={loading} />;
  }
}

export default App;
