const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const fs = require('fs');
const path = require('path');

app.use(bodyParser());
app.use(cors());
app.use('/files', express.static(path.join(__dirname, 'uploads')));

app.get('/list', function(req, res) {
  const route = path.join(__dirname, '/uploads');
  fs.readdir(route, function(err, file) {
    if (err) {
      return res.status(500).send(err);
    }

    res.send(JSON.stringify(file));
  });
});

app.listen(8000, function() {
  console.log('Express server listening on port 8000');
});
